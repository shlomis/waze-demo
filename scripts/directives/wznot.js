'use strict';

angular.module('wazeApp')
    .directive('wzNot', function (Notifications) {
        return {
            templateUrl: '/views/directives/note.html',
            restrict: 'A',
            scope: {
                notification: "="
            },
            link: function postLink(scope, element, attrs) {
                /**
                 * delegates for the notification directive
                 */
                scope.save = function(){
                    Notifications.save(scope.notification);
                };

                scope.update = function(){
                    Notifications.update(scope.notification);
                };

                scope.delete = function(){
                    Notifications.delete(scope.notification);
                };

                scope.toggleVote = function(){
                    if (!scope.notification.voted) {
                        scope.notification.voted = true;
                        Notifications.upVote(scope.notification);
                    }
                };
            }
        };
    });
