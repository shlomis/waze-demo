Waze - geo-location live notifications system
=========

This project was built using the following:

  - Yeoman Scaffolding tool
  - Grunt
  - Bower
  - expressJS
  - generator-angular-fullstack (https://github.com/DaftMonk/generator-angular-fullstack)

The application repository is extracted from the build project (for simplicity sake).

> The application uses the approach, of breaking down the UI units into independent directives with 
> isolated scopes. All directives share a single notifications list referance. The AJAX calls are beeing proxy-pass thrugh
> the express server routes.


Routes code extracted
--------------


```js
'use strict';

var api = require('./controllers/api'),
    index = require('./controllers'),
    request = require('request');


/**
 * Application routes
 */
module.exports = function (app) {
    // All undefined api routes should return a 404
    app.get('/notifications.json', function (req, res) {
        req.pipe(request("http://test-notifications.staging.waze.com/notifications.json")).pipe(res);
    });

    app.post('/notifications.json', function (req, res) {
        req.pipe(request.post("http://test-notifications.staging.waze.com/notifications.json", {form: req.body})).pipe(res);
    });

    app.put('/notifications/:id.json', function (req, res) {
        req.pipe(request.put("http://test-notifications.staging.waze.com/notifications/" + req.route.params.id + ".json", {form: req.body})).pipe(res);
    });

    app.put('/notifications/:id/upvote.json', function (req, res) {
        req.pipe(request.put("http://test-notifications.staging.waze.com/notifications/" + req.route.params.id + "/upvote.json", {form: req.body})).pipe(res);
    });

    app.delete('/notifications/:id.json', function (req, res) {
        req.pipe(request("http://test-notifications.staging.waze.com/notifications/" + req.route.params.id + ".json", {form: req.body})).pipe(res);
    });

    // All other routes to use Angular routing in app/scripts/app.js
    app.get('/partials/*', index.partials);
    app.get('/*', index.index);
};
```
Live demo
----
http://54.186.178.246:9000/