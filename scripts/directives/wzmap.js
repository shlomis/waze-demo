'use strict';

angular.module('wazeApp')
    .directive('wzMap', function (Notifications, $compile) {
        var uid = "map_" + Math.round(Math.random() * 10000).toString(16);

        return {
            template: '<div class="map" id="' + uid + '"></div>',
            restrict: 'E',
            scope: {
                notifications: "="
            },
            link: function postLink(scope, element, attrs) {

                /**
                 * initiates the map on a given DIV and locates the user geo location
                 */
                function initMap() {
                    scope.map = L.map(uid).setView([scope.mapData.latitude, scope.mapData.longitude], scope.mapData.zoom);

                    // add an OpenStreetMap tile layer
                    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    }).addTo(scope.map);

                    scope.map.locate({setView: true, maxZoom: scope.mapData.zoom});

                    scope.map.whenReady(function () {
                        scope.$emit('mapready', scope.map);
                    });
                }

                /**
                 * adds a marker when clicked on the map
                 * @param e
                 */
                function onMapClicked(e) {
                    scope.$apply(function () {
                        if (scope.dataRecived) {
                            var cords = e.latlng;
                            var notification = Notifications.getInstance(cords.lng, cords.lat);
                            addMarker(notification);
                            notification.showPopup();
                            scope.$emit('mapclick', scope.map);
                            scope.map.setView([notification.lat, notification.lon], scope.mapData.zoom);
                        }
                    });
                }

                /**
                 * adds all interface listeners
                 */
                function addListeners() {
                    scope.map.on('click', onMapClicked);
                    scope.map.on('moveend', function (e) {
                        if (!scope.$parent.$$phase) {
                            scope.$apply(function () {
                                scope.$emit('moveend', scope.map);
                            });
                        }
                    });
                    scope.$on('notificationsData', onNotificationData);
                }

                /**
                 * triggered when data is retrieved from the staging server
                 * @param e
                 * @param notifications
                 */
                function onNotificationData(e, notifications) {
                    scope.dataRecived = true;
                    var notification;
                    for (var i = 0; i < notifications.length; i++) {
                        notification = notifications[i];
                        addMarker(notification);
                    }
                    scope.map.setView([notification.lat, notification.lon], scope.mapData.zoom);
                    scope.$emit('moveend', scope.map);
                }

                /**
                 * adds a new marker on the map and attach a notification object
                 * @param notification
                 */
                function addMarker(notification) {
                    //unique id for the directive
                    var uid = "not_" + Math.round(Math.random() * 10000).toString(16);

                    // saving reference for quick access
                    scope.notificationsMap[uid] = notification;
                    notification.marker = L.marker([notification.lat || 0, notification.lon || 0]).addTo(scope.map).bindPopup('<div id="' + uid + '" wz-not notification="notificationsMap[\'' + uid + '\']"></div>', {maxHeight: 170, minWidth: 300});

                    notification.showPopup = function () {
                        notification.marker.openPopup();
                        $compile($("#" + uid))(scope);
                        scope.map.setView([notification.lat, notification.lon], scope.mapData.zoom);
                    };

                    notification.hidePopup = function () {
                        notification.marker.closePopup();
                        $compile($("#" + uid))(scope);
                    };

                    notification.removeFromList = function () {
                        for (var i = 0; i < scope.notifications.length; i++) {
                            if (notification === scope.notifications[i]) {
                                scope.map.removeLayer(notification.marker);
                                scope.notifications.splice(i, 1);
                                break;
                            }
                        }
                    };

                    //compiling the added html
                    $compile($("#" + uid))(scope);
                    scope.notifications.push(notification);

                    notification.marker.on('click', notification.showPopup);
                }

                /**
                 * fires up the directive
                 */
                function init() {
                    initMap();
                    addListeners();
                }

                // setting Ny as the default location
                scope.mapData = {
                    latitude: 40.732169,
                    longitude: -73.993263,
                    zoom: 13
                };

                // a map for quick access
                scope.notificationsMap = {};

                init();

            }
        };
    });
