'use strict';

angular.module('wazeApp')
    .directive('wzNotList', function () {
        return {
            templateUrl: '/views/directives/noteList.html',
            restrict: 'E',
            scope: {
                notifications: "="
            },
            link: function postLink(scope, element, attrs) {
                /**
                 * toggles the notification panel on click
                 * @param e
                 * @param notification
                 */
                scope.toggleCollapse = function (e , notification) {
                    var noteSection = $("#" + $(e.currentTarget).attr("data-collapse"));
                    noteSection.toggleClass("in");
                    if (noteSection.hasClass("in")) {
                        notification.showPopup();
                    } else {
                        notification.hidePopup();
                    }

                };

                /**
                 * toggles the list view panel
                 * @param e
                 */
                scope.togglePanel = function(e){
                    $(".noteListWrapper").toggleClass("in");
                };
            }
        };
    });
