'use strict';

angular.module('wazeApp').controller('MainCtrl', function ($scope, $http, Notifications) {
    $scope.notifications = [];

    // gets the initial notifications data
    $http.get('/notifications.json').success(function (res) {
        for (var i = 0; i < res.length; i++) {
            var notification = res[i];
            if (notification.is_active) {
                notification.lat = notification.lat || 0;
                notification.lon = notification.lon || 0;
                notification.cords = new L.LatLng(notification.lat, notification.lon);
            } else {
                res.splice(i, 1);
                i--;
            }
        }
        $scope.$broadcast("notificationsData", res);
        $scope.dataReceived = true;
    });

    //triggered when the map directive is ready
    $scope.$on('mapready', function (e, map) {
        $scope.map = map;
    });

    $scope.$on('moveend', function (e) {
        updateNotifications();
    });

    $scope.$on('mapclick', function (e) {
        updateNotifications();
    });

    //calls the notification server to update the interface data
    function updateNotifications() {
        Notifications.updateCurrentData($scope.notifications, $scope.map);
        $scope.visibleNotifications = (Notifications.visibleCounter === $scope.notifications.length) ? "All Notifications" : Notifications.visibleCounter + " of " + $scope.notifications.length + " Notifications";
    }

});
