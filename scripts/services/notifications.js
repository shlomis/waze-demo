'use strict';

angular.module('wazeApp')
    .service('Notifications', function Notifications($http) {
        // AngularJS will instantiate a singleton by calling "new" on this function

        function shallowCopy(src, dst) {
            dst = dst || {};

            for (var key in src) {
                if (src.hasOwnProperty(key)) {
                    dst[key] = src[key];
                }
            }

            return dst;
        }

        /**
         * Checks if a notification is visible
         * @param notification
         * @param map
         * @returns boolean
         */
        function isVisible(notification, map) {
            var bounds = map.getBounds();
            return bounds.contains(notification.cords);
        }

        /**
         *  returns the distance between a notification to the mapCenter of the map
         * @param notification
         * @param map
         * @returns {number}
         */
        function getDistance(notification, map) {
            var mapCenter = map.getCenter();

            var radLat = convertToRad(mapCenter.lat);
            var delta = convertToRad(mapCenter.lng - notification.lon);

            var dist = Math.sin(radLat) * Math.sin(radLat) + Math.cos(radLat) * Math.cos(radLat) * Math.cos(delta);
            dist = Math.acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;
            return dist;
        }

        /**
         * Util convert to rad
         * @param deg
         * @returns {rad number}
         */
        function convertToRad(deg) {
            var pi = Math.PI;
            return  pi * deg / 180;
        }


        /**
         * returns the notification form as string
         * @param note
         * @returns {*}
         */
        function getFormData(note) {
            return $.param({
                "notification[title]": note.title,
                "notification[description]": note.description,
                "notification[lon]": note.lon,
                "notification[lat]": note.lat
            });
        }

        //the actual service interface
        return {
            visibleCounter: 0,
            getInstance: function (lon, lat) {
                return {
                    lon: lon,
                    lat: lat,
                    cords: new L.LatLng(lat, lon),
                    title: "My Title",
                    description: "My Description",
                    votes_up: 0,
                    voted: false,
                    saved: false
                };
            },
            updateCurrentData: function (notifications, map) {
                this.visibleCounter = 0;
                for (var i = 0; i < notifications.length; i++) {
                    notifications[i].isVisible = isVisible(notifications[i], map);
                    notifications[i].distance = getDistance(notifications[i], map);
                    this.visibleCounter += notifications[i].isVisible ? 1 : 0;
                }
            },
            save: function (note) {
                $http({
                    method: 'POST',
                    url: '/notifications.json',
                    data: getFormData(note),
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).success(function (created) {
                        shallowCopy(created, note);
                    });
            },
            update: function (note) {
                $http({
                    method: 'PUT',
                    url: '/notifications/' + note.id + '.json',
                    data: getFormData(note),
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).success(function (created) {
                        shallowCopy(created, note);
                    });
            },
            delete: function (note) {
                if (confirm("Are you sure you want to delete " + note.title + " ?")) {
                    $http({
                        method: 'DELETE',
                        url: '/notifications/' + note.id + '.json',
                        data: getFormData(note),
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    }).success(function () {
                            note.removeFromList();
                        });
                }
            },
            upVote: function (note) {
                $http({
                    method: 'PUT',
                    url: '/notifications/' + note.id + '/upvote.json',
                    data: getFormData(note),
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).success(function (created) {
                        shallowCopy(created, note);
                    });
            }
        };
    });


